# Convert regular JSON file to PostGIS records
import urllib.request
import pandas as pd
import geopandas as gpd
from shapely.geometry import Point


class JSONtoPostGIS:
    def __init__(self):
        self.json_source = ''    # JSON URL
        self.df = pd.DataFrame()   # JSON dataframe

    def set_source(self, json_source):
        self.json_source = json_source

    def read_json(self):
        data = urllib.request.urlopen(self.json_source).read()
        self.df = pd.read_json(data)
        return self.df

    def create_gdf(self, type, longitude='longitude', latitude='latitude',
                   geom_col='geom'):
        self.geom_col = geom_col
        if type == 'point':
            self.df[geom_col] = list(zip(self.df[longitude], self.df[latitude]))
            self.df[geom_col] = self.df[geom_col].apply(Point)
        self.gdf = gpd.GeoDataFrame(self.df, geometry=geom_col)

    def drop_columns(self, columns_to_drop):
        if not isinstance(columns_to_drop, list):
            raise TypeError('columns_to_drop must be a list')
        for column in columns_to_drop:
            self.gdf = self.gdf.drop([column], axis=1)


if __name__ == '__main__':
    JSON_URL = 'https://data.cityofchicago.org/resource/4ywc-hr3a.json'
    jtp = JSONtoPostGIS()
    jtp.set_source(JSON_URL)
    jtp.read_json()
    jtp.create_gdf(type='point', longitude='longitude', latitude='latitude')
    jtp.drop_columns(['location'])
    jtp.gdf = jtp.gdf[['rackid', 'community_name', 'ward', 'geom']]
    jtp.gdf.to_file('src/data/bike_racks.geojson', driver='GeoJSON')

    bike_buffer = jtp.gdf
    bike_buffer.crs = {'init': 'epsg:4326'}
    bike_buffer = bike_buffer.to_crs({'init': 'epsg:3435'})
    bike_buffer = bike_buffer.buffer(200)
    bike_buffer = bike_buffer.to_crs({'init': 'epsg:4326'})
    bike_buffer.to_file('src/data/bike_buffer.geojson', driver='GeoJSON')